import abc


class AmbiguousOptionError(Exception):
    pass


class NoMatchingOptionFound(AttributeError):
    pass


class ConfigError(Exception):
    pass


class ConflictConfigError(ConfigError):
    pass


class Config(object):
    _cfgimpl_frozen = False

    def __init__(self, description, parent=None, **overrides):
        self._cfgimpl_description = description
        self._cfgimpl_value_owners = {}
        self._cfgimpl_parent = parent
        self._cfgimpl_values = {}
        self._cfgimpl_warnings = []
        self._cfgimpl_build(overrides)

    def _cfgimpl_build(self, overrides):
        for child in self._cfgimpl_description._children:
            if isinstance(child, Option):
                self._cfgimpl_values[child._name] = child.get_default()
                self._cfgimpl_value_owners[child._name] = 'default'
            elif isinstance(child, OptionDescription):
                self._cfgimpl_values[child._name] = Config(child, parent=self)
        self.override(overrides)

    def override(self, overrides):
        for name, value in overrides.items():
            home_config, name = self._cfgimpl_get_home_by_path(name)
            home_config.setoption(name, value, 'default')

    def copy(self, as_default=False, parent=None):
        result = Config.__new__(self.__class__)
        result._cfgimpl_description = self._cfgimpl_description
        result._cfgimpl_value_owners = owners = {}
        result._cfgimpl_parent = parent
        result._cfgimpl_values = v = {}
        for child in self._cfgimpl_description._children:
            if isinstance(child, Option):
                v[child._name] = self._cfgimpl_values[child._name]
                if as_default:
                    owners[child._name] = 'default'
                else:
                    owners[child._name] = (
                        self._cfgimpl_value_owners[child._name])
            elif isinstance(child, OptionDescription):
                v[child._name] = self._cfgimpl_values[child._name].copy(
                    as_default, parent=result)
        return result

    def __setattr__(self, name, value):
        if self._cfgimpl_frozen and getattr(self, name) != value:
            raise TypeError("trying to change a frozen option object")
        if name.startswith('_cfgimpl_'):
            self.__dict__[name] = value
            return
        self.setoption(name, value, 'user')

    def __getattr__(self, name):
        if '.' in name:
            homeconfig, name = self._cfgimpl_get_home_by_path(name)
            return getattr(homeconfig, name)
        if name.startswith('_cfgimpl_'):
            # if it were in __dict__ it would have been found already
            raise AttributeError("%s object has no attribute %s" %
                                 (self.__class__, name))
        if name not in self._cfgimpl_values:
            raise AttributeError("%s object has no attribute %s" %
                                 (self.__class__, name))
        return self._cfgimpl_values[name]

    def __dir__(self):
        from_type = dir(type(self))
        from_dict = list(self.__dict__)
        extras = list(self._cfgimpl_values)
        return sorted(set(extras + from_type + from_dict))

    def __delattr__(self, name):
        # XXX if you use delattr you are responsible for all bad things
        # happening
        if name.startswith('_cfgimpl_'):
            del self.__dict__[name]
            return
        self._cfgimpl_value_owners[name] = 'default'
        opt = getattr(self._cfgimpl_description, name)
        if isinstance(opt, OptionDescription):
            raise AttributeError("can't option subgroup")
        self._cfgimpl_values[name] = getattr(opt, 'default', None)

    def setoption(self, name, value, who):
        if name not in self._cfgimpl_values:
            raise AttributeError('unknown option %s' % (name,))
        child = getattr(self._cfgimpl_description, name)
        old_owner = self._cfgimpl_value_owners[child._name]
        if old_owner not in ("default", "suggested"):
            old_value = getattr(self, name)
            if old_value == value or who in ("default", "suggested"):
                return
            raise ConflictConfigError('cannot override value to %s for '
                                      'option %s' % (value, name))
        child.setoption(self, value, who)
        self._cfgimpl_value_owners[name] = who

    def suggest(self, **kwargs):
        for name, value in kwargs.items():
            self.suggest_option(name, value)

    def suggest_option(self, name, value):
        try:
            self.setoption(name, value, "suggested")
        except ConflictConfigError:
            # setting didn't work, but that is fine, since it is
            # suggested only
            pass

    def set(self, **kwargs):
        all_paths = [p.split(".") for p in self.getpaths()]
        for key, value in kwargs.items():
            key_p = key.split('.')
            candidates = [p for p in all_paths if p[-len(key_p):] == key_p]
            if len(candidates) == 1:
                name = '.'.join(candidates[0])
                home_config, name = self._cfgimpl_get_home_by_path(name)
                home_config.setoption(name, value, "user")
            elif len(candidates) > 1:
                raise AmbiguousOptionError(
                    'more than one option that ends with %s' % (key, ))
            else:
                raise NoMatchingOptionFound(
                    'there is no option that matches %s' % (key, ))

    def _cfgimpl_get_home_by_path(self, path):
        """returns tuple (config, name)"""
        path = path.split('.')
        conf = self
        for step in path[:-1]:
            conf = getattr(self, step)
        return conf, path[-1]

    def _cfgimpl_get_top_level(self):
        conf = self
        while self._cfgimpl_parent is not None:
            conf = self._cfgimpl_parent
        return conf

    def add_warning(self, warning):
        self._cfgimpl_get_top_level()._cfgimpl_warnings.append(warning)

    def get_warnings(self):
        return self._cfgimpl_get_top_level()._cfgimpl_warnings

    def _freeze_(self):
        self.__dict__['_cfgimpl_frozen'] = True
        return True

    def getkey(self):
        return self._cfgimpl_description.getkey(self)

    def __hash__(self):
        return hash(self.getkey())

    def __eq__(self, other):
        return self.getkey() == other.getkey()

    def __ne__(self, other):
        return not self == other

    def __iter__(self):
        for child in self._cfgimpl_description._children:
            if isinstance(child, Option):
                yield child._name, getattr(self, child._name)

    def __str__(self, indent=""):
        lines = []
        children = [(child._name, child)
                    for child in self._cfgimpl_description._children]
        children.sort()
        for name, child in children:
            if self._cfgimpl_value_owners.get(name, None) == 'default':
                continue
            value = getattr(self, name)
            if isinstance(value, Config):
                substr = value.__str__(indent + "    ")
            else:
                substr = "%s    %s = %s" % (indent, name, value)
            if substr:
                lines.append(substr)
        if indent and not lines:
            return ''   # hide subgroups with all default values
        lines.insert(0, "%s[%s]" % (indent, self._cfgimpl_description._name,))
        return '\n'.join(lines)

    def getpaths(self, include_groups=False):
        """returns a list of all paths in self, recursively
        """
        return self._cfgimpl_description.getpaths(include_groups=include_groups)


_NO_DEFAULT_MARKER = object()  # Don't touch or access this value. Just don't.


class Option(object):
    """
    Abstract base class for configuration options.
    """

    __metaclass__ = abc.ABCMeta

    def __init__(self, name, help, default=_NO_DEFAULT_MARKER, validators=()):
        """
        Initialize new Option.

        :param name: The name of the option. Should be a valid python identifier
            name.
        :param help: A human-readable string describing this option.
        :param default: Default value for this option. Can be anything as long
            as it is a valid value for this option. (See the `validators` param)
        :param validators: A list of `.validator.Validator` objects. A value is
            considered valid only if all of the registered validators returned
            `True`.
        """
        self._name = name
        self.doc = help
        self.validators = validators
        self.default = default

    def validate(self, value):
        """
        Validate a value for this option. This is done by iterating over the
        `validators` list, and ANDing their results. Thus a value is considered
        valid only if **ALL** of the registered validators returned `True`.

        :param value: The value to validate.
        :return: Whether the value is valid or not.
        :rtype: bool.
        """
        for validator in self.validators:
            if not validator(self, value):
                return False
        return True

    def get_default(self):
        if self.default is _NO_DEFAULT_MARKER:
            raise ValueError("{}({}) does not have a default value".format(
                self.__class__.__name__, self._name))
        return self.default

    def setoption(self, config, value, who):
        name = self._name
        if who == "default" and value is None:
            pass
        elif not self.validate(value):
            raise ConfigError('invalid value %s for option %s' % (value, name))
        config._cfgimpl_values[name] = value

    def getkey(self, value):
        return value

    def convert_from_cmdline(self, value):
        return value


class ChoiceOption(Option):

    def __init__(self, name, help, values, default=None, requires=None,
                 suggests=None):
        super(ChoiceOption, self).__init__(name, help)
        self.values = values
        self.default = default
        if requires is None:
            requires = {}
        self._requires = requires
        if suggests is None:
            suggests = {}
        self._suggests = suggests

    def setoption(self, config, value, who):
        name = self._name
        for path, reqvalue in self._requires.get(value, []):
            toplevel = config._cfgimpl_get_top_level()
            homeconfig, name = toplevel._cfgimpl_get_home_by_path(path)
            if who == 'default':
                who2 = 'default'
            else:
                who2 = 'required'
            homeconfig.setoption(name, reqvalue, who2)
        for path, reqvalue in self._suggests.get(value, []):
            toplevel = config._cfgimpl_get_top_level()
            homeconfig, name = toplevel._cfgimpl_get_home_by_path(path)
            homeconfig.suggest_option(name, reqvalue)
        super(ChoiceOption, self).setoption(config, value, who)

    def validate(self, value):
        return value is None or value in self.values

    def convert_from_cmdline(self, value):
        return value.strip()


class BoolOption(Option):
    def __init__(self, name, help, default=None, requires=None,
                 suggests=None, validator=None, negation=True):
        super(BoolOption, self).__init__(name, help)
        self._requires = requires
        self._suggests = suggests
        self.default = default
        self.negation = negation
        self._validator = validator

    def validate(self, value):
        return isinstance(value, bool)

    def setoption(self, config, value, who):
        name = self._name
        if value and self._validator is not None:
            toplevel = config._cfgimpl_get_top_level()
            self._validator(toplevel)
        if value and self._requires is not None:
            for path, reqvalue in self._requires:
                toplevel = config._cfgimpl_get_top_level()
                homeconfig, name = toplevel._cfgimpl_get_home_by_path(path)
                if who == 'default':
                    who2 = 'default'
                else:
                    who2 = 'required'
                homeconfig.setoption(name, reqvalue, who2)
        if value and self._suggests is not None:
            for path, reqvalue in self._suggests:
                toplevel = config._cfgimpl_get_top_level()
                homeconfig, name = toplevel._cfgimpl_get_home_by_path(path)
                homeconfig.suggest_option(name, reqvalue)

        super(BoolOption, self).setoption(config, value, who)


class IntOption(Option):

    def __init__(self, name, help, default=None):
        super(IntOption, self).__init__(name, help)
        self.default = default

    def validate(self, value):
        try:
            int(value)
        except TypeError:
            return False
        return True

    def setoption(self, config, value, who):
        try:
            super(IntOption, self).setoption(config, int(value), who)
        except TypeError as e:
            raise ConfigError(*e.args)


class FloatOption(Option):

    def __init__(self, name, help, default=None):
        super(FloatOption, self).__init__(name, help)
        self.default = default

    def validate(self, value):
        try:
            float(value)
        except TypeError:
            return False
        return True

    def setoption(self, config, value, who):
        try:
            super(FloatOption, self).setoption(config, float(value), who)
        except TypeError as e:
            raise ConfigError(*e.args)


class StrOption(Option):

    def __init__(self, name, help, default=None):
        super(StrOption, self).__init__(name, help)
        self.default = default

    def validate(self, value):
        return isinstance(value, str)

    def setoption(self, config, value, who):
        try:
            super(StrOption, self).setoption(config, value, who)
        except TypeError as e:
            raise ConfigError(*e.args)


class ArbitraryOption(Option):
    def __init__(self, name, help, default=None, defaultfactory=None):
        super(ArbitraryOption, self).__init__(name, help)
        self.default = default
        self.defaultfactory = defaultfactory
        if defaultfactory is not None:
            assert default is None

    def validate(self, value):
        return True

    def get_default(self):
        if self.defaultfactory is not None:
            return self.defaultfactory()
        return self.default


class OptionDescription(object):

    cmdline = None

    def __init__(self, name, doc, children):
        self._name = name
        self.doc = doc
        self._children = children
        self._build()

    def _build(self):
        for child in self._children:
            setattr(self, child._name, child)

    def getkey(self, config):
        return tuple([child.getkey(getattr(config, child._name))
                      for child in self._children])

    def getpaths(self, include_groups=False, currpath=None):
        """returns a list of all paths in self, recursively

            currpath should not be provided (helps with recursion)
        """
        if currpath is None:
            currpath = []
        paths = []
        for option in self._children:
            attr = option._name
            if attr.startswith('_cfgimpl'):
                continue
            value = getattr(self, attr)
            if isinstance(value, OptionDescription):
                if include_groups:
                    paths.append('.'.join(currpath + [attr]))
                currpath.append(attr)
                paths += value.getpaths(include_groups=include_groups,
                                        currpath=currpath)
                currpath.pop()
            else:
                paths.append('.'.join(currpath + [attr]))
        return paths


class ConfigUpdate(object):

    def __init__(self, config, option):
        self.config = config
        self.option = option

    def convert_from_cmdline(self, value):
        return self.option.convert_from_cmdline(value)

    def __call__(self, option, opt_str, value, parser, *args, **kwargs):
        value = self.convert_from_cmdline(value)
        self.config.setoption(self.option._name, value, who='cmdline')

    def help_default(self):
        default = getattr(self.config, self.option._name)
        owner = self.config._cfgimpl_value_owners[self.option._name]
        if default is None:
            if owner == 'default':
                return ''
            else:
                default = '???'
        return "%s: %s" % (owner, default)


class BoolConfigUpdate(ConfigUpdate):
    def __init__(self, config, option, which_value):
        super(BoolConfigUpdate, self).__init__(config, option)
        self.which_value = which_value

    def convert_from_cmdline(self, value):
        return self.which_value

    def help_default(self):
        default = getattr(self.config, self.option._name)
        owner = self.config._cfgimpl_value_owners[self.option._name]
        if default == self.which_value:
            return owner
        else:
            return ""


def make_dict(config):
    paths = config.getpaths()
    options = dict([(path, getattr(config, path)) for path in paths])
    return options
