import abc


class Validator(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __call__(self, value):
        """
        Validate a value for an option.

        :param value: The value to validate.
        :return: Boolean. `True` if the value is valid, False otherwise.
        :rtype: bool
        """


class RangeValidator(Validator):
    """
    Validator for number types. Validates that a given value is withing a given
    range.
    """

    def __init__(self, min_value=None, max_value=None):
        """
        Initialize a new `RangeValidator`.

        :param min_value: Lower limit. **Inclusive**. May be omitted if only
            upper limit is desired.
        :param max_value: Upper limit. **Inclusive** May be omitted if only
            lower limit is desired.

        Notice that **at least** one limit value must be provided, or a
        `ValueError` will be raised
        """
        if min_value is None and max_value is None:
            raise ValueError("At least one limit must be provided")
        self._min = min_value
        self._max = max_value

    def __call__(self, value):
        if self._min is not None and value < self._min:
            return False

        if self._max is not None and value > self._max:
            return False
        return True
