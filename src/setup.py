from glob import glob

from os.path import join, dirname, splitext, basename
from setuptools import setup, find_packages


PACKAGE_NAME = "peyconfig"


def read(*path):
    return open(join(dirname(__file__), *path)).read()


version = {}
exec(
    read("src", PACKAGE_NAME, "version.py"),
    version
)  # Now we have `__version__` in `version`

setup(
    name=PACKAGE_NAME,
    version=version["__version__"],
    packages=find_packages('src'),
    package_dir={'': 'src'},
    py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
    url='https://gitlab.com/avrahamshukron/peyconfig',
    license='MIT',
    author='Avraham Shukron',
    author_email='avraham.shukron@gmail.com',
    description='Configuration management library written in Python',
    long_description=read("README.md"),
    include_package_data=True,
)
